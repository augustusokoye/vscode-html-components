'use strict';

import * as vscode from 'vscode';
import * as path from 'path';
import * as glob from 'glob';
import * as fs from 'fs';
import { Helper, FileHelper, IFileResolver, Component } from './Helper';
import { StatusBarUi } from './StatubarUi';
import { OutputWindow } from './OuputWindow';

var htmlMinify = require('html-minify');
var mustache = require('mustache');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;


export class App {
    html_components_dir: string;
    isWatching: boolean;
    buildStartTime: Date;

    constructor() {
        this.html_components_dir = '';
        this.isWatching = false;
        StatusBarUi.init();
        this.buildStartTime = new Date(0);
    }

    static get basePath(): string {
        return vscode.workspace.rootPath || path.basename(vscode.window.activeTextEditor.document.fileName);
    }

    /**
     * Build all components
     * @param WatchingMode WatchingMode = false for without watch mode.
     */
    buildAllComponents() {

        let showOutputWindow = Helper.getConfigSettings<boolean>('showOutputWindow');
        this.buildStartTime = new Date();

        this.generateAllHTML(showOutputWindow).then(() => {
            //
        });
    }

    async buildOnSave() {
        if (!this.isWatching) return;

        let currentFile = vscode.window.activeTextEditor.document.fileName;

        if (this.isComponentFile(currentFile)){
            OutputWindow.Show('Change Detected...', [path.basename(currentFile)], false, false);

            this.buildStartTime = new Date();

            currentFile = currentFile.replace('.component.html', '.component.json')

            let componentDetails = await this.getComponent(currentFile);
            this.generateHTML(componentDetails).then(e => {
                let info = Array();
                info.push(e);
                let now = new Date();
                info.push(`Duration: ${Helper.Instance.timeDifference_ms(this.buildStartTime, now)} ms`);
                info.push(Helper.Instance.timeInfo(now));
                OutputWindow.Show('Generated:', info, false, false);
                OutputWindow.Show(null, null, false, true);
                OutputWindow.Show('Watching...', null);
            });
        }
        else if(this.isComponentMemberFile(currentFile)){
            if(Helper.getConfigSettings<boolean>('autoSaveAll')){
                await vscode.workspace.saveAll();
            }
            this.buildAllComponents();
        }
        else return;
    }

    startWatch(){

        if (this.isWatching) {
            vscode.window.showInformationMessage('HTML Component: Already watching...');
            return;
        }

        this.buildAllComponents();
        this.toggleStatusUI();
    }

    stopWatch() {
        if (this.isWatching) {
            this.toggleStatusUI();
        }else {
            vscode.window.showInformationMessage('HTML Component: Not watching...');
        }
    }

    isComponentFile(pathUrl: string): boolean {
        const filename = path.basename(pathUrl);
        const dirName = pathUrl.replace(filename,'');

        if(!filename.startsWith('_') && filename.includes('.component.')){
            if(fs.existsSync(path.join(dirName, filename.replace('.component.json', '.component.html'))) &&
               fs.existsSync(path.join(dirName, filename.replace('.component.html', '.component.json')))
            ) return true;
        }
        return false;
    }

    isComponentIndex(pathUrl: string): boolean {
        const filename = path.basename(pathUrl);
        const dirName = pathUrl.replace(filename,'');

        return (!filename.startsWith('_') &&
                (filename == 'index.html') &&
                dirName.includes('html_components'));
    }

    isComponentTemplate(pathUrl: string): boolean {
        const filename = path.basename(pathUrl);
        const dirName = pathUrl.replace(filename,'');

        return (!filename.startsWith('_') &&
                (dirName.includes('html_components/templates') || dirName.includes('html_components\\templates')));
    }

    isComponentMemberFile(pathUrl: string): boolean {
        const filename = path.basename(pathUrl);
        const dirName = pathUrl.replace(filename,'');

        return (!filename.startsWith('_') &&
                dirName.includes('html_components'));
    }

    /**
     * Get component files in the html_components/*** directory that match pattern
     * @param WatchingMode WatchingMode = false for without watch mode.
     */
    async getComponentFiles(fileExt = 'json'): Promise<string[]> {

        let componentsDirDepth = 4;

        var files = new Array();

		let temp = Array(componentsDirDepth+1).fill('');
		const indexFileUriPatterns = temp.map((e, i) => {
            return '*/html_components'+'/*'.repeat(i+1)+'.component.'+fileExt;
		});

        for(var i=0; i<indexFileUriPatterns.length; i++){
            let dirFiles = await this.getFiles(indexFileUriPatterns[i]);
            for(var j=0; j<dirFiles.length; j++){
                files.push(dirFiles[j]);
            }
        }

        return new Promise(resolve => {
            if(files.length){
                return resolve(files || []);
            }else{
                resolve([]);
                return;
            }
        })
    }




    /**
     * Build all components
     * @param popUpOutputWindow To control output window.
     */
    private generateAllHTML(popUpOutputWindow: boolean) {

        return new Promise((resolve) => {
            this.getAllComponentsAsync((componentDetails: Component[]) => {

                let componentNames = componentDetails.map(detail => detail.name);
                OutputWindow.Show('Building Components: ', componentNames, popUpOutputWindow);
                let promises = new Array();
                componentDetails.forEach(omponentDetail => {
                    promises.push(this.generateHTML(omponentDetail));
                });

                Promise.all(promises).then((e: string[]) => {
                    let now = new Date();
                    e.push(`Duration: ${Helper.Instance.timeDifference_ms(this.buildStartTime, now)} ms`);
                    e.push(Helper.Instance.timeInfo(now));
                    OutputWindow.Show('Generated:', e, false, false);
                    OutputWindow.Show(null, null, false, true);
                    resolve(e);
                });
            });
        });
    }




    /**
     * To Generate one HTML output from Component
     * @param component - Component Details
     * @param targetUri -  Target HTML file URI (string)
     */
    private generateHTML(component: Component) {

        let showOutputWindow = Helper.getConfigSettings<boolean>('showOutputWindow');

        return new Promise(resolve => {
            let targetUri = this.generateHTMLUri(component.dest, component.loc, component.target);

            this.getFiles('*/html_components/index.html').then(indexUrl => {

                fs.readFile(indexUrl.toString(), (err, indexData) => {
                    if(err){
                        // Error
                        //OutputWindow.Show('Build Error', [`Something went wrong with Component ${component.name}`], showOutputWindow);
                        OutputWindow.Show('Build Error:', [`Something went wrong with Component ${component.name}`, "Could not read Component mainframe. Please add an index.html to the html_components folder."], showOutputWindow, true);
                        StatusBarUi.buildError(this.isWatching);
                    }else{

                        let componentTarget = indexData.toString();

                        // add component html
                        let  componentHtml = fs.readFileSync(component.htmlUri).toString();
                        componentTarget = Helper.Instance.place(componentTarget, component.selector, componentHtml);

                        // add templates
                        if(component.templatesUri){
                            component.templatesUri.forEach(templateUri => {
                                let thisSource = path.join(this.html_components_dir, 'templates', templateUri.source);
                                if(fs.existsSync(thisSource)){
                                    let  templateHtml = fs.readFileSync(thisSource).toString();
                                    componentTarget = Helper.Instance.place(componentTarget, templateUri.selector, templateHtml);
                                }else{
                                    // Error
                                    OutputWindow.Show('Warning:', [thisSource, `This template does not exist for ${component.name}`], false, false);
                                }
                            });
                        }

                        // add script and link tags
                        if(component.tags){
                            component.tags.forEach(tag => {
                                let selector = tag.selector;
                                if(componentTarget.includes(selector)){

                                    let tags = Array();

                                    if(tag.links)
                                        tag.links.forEach(link => tags.push(`<link rel="stylesheet" type="text/css" href="${link}">`));
                                    tags.push(''); // Add separation
                                    if(tag.scripts)
                                        tag.scripts.forEach(script => tags.push(`<script src="${script}"></script>`));
                                    tags.push(''); // Add separation
                                    if(tag.any)
                                        tag.any.forEach(anyTag => {
                                            let tagAttributes = '';
                                            if(anyTag.attributes){
                                                anyTag.attributes.forEach(attr => {
                                                    tagAttributes += `${attr.attributeLabel}="${attr.value}" `;
                                                });
                                            }
                                            tags.push(`<${anyTag.tag} ${tagAttributes}></${anyTag.tag}>`)
                                        });

                                    componentTarget = Helper.Instance.placeLines(componentTarget, selector, tags);

                                }else{
                                    // Error
                                    OutputWindow.Show('Warning:', [`Selector ${selector} not found for ${component.name}`], false, false);
                                }
                            })

                        }

                        // place inlines
                        if(component.inlines){
                            component.inlines.forEach(inline => {
                                let thisSource = inline.source;
                                if (inline.source.startsWith('~')){
                                    inline.source = inline.source.replace("~/", "/");
                                    thisSource = path.join(component.loc, inline.source);
                                }
                                else{
                                    //thisSource = path.join(this.html_components_dir, inline.source);
                                    thisSource = path.join(vscode.workspace.rootPath, inline.source);
                                }

                                //let thisSource = path.join(vscode.workspace.rootPath, inline.source);
                                if(fs.existsSync(thisSource)){
                                    let attributeLabel = '';
                                    if(inline.attributes){
                                        inline.attributes.forEach(attr => {
                                            attributeLabel += `${attr.attributeLabel}="${attr.value}" `;
                                        });
                                    }
                                    let  inlineData = `<script ${attributeLabel}>\n${fs.readFileSync(thisSource).toString()}\n</script>`;
                                    componentTarget = Helper.Instance.place(componentTarget, inline.selector, inlineData);
                                }else{
                                    // Error
                                    OutputWindow.Show('Warning:', [thisSource, `This source could not be resolved as inline script for ${component.name}`], false, false);
                                }
                            });
                        }

                        // add variables
                        if(component.variables){
                            let obj = {};
                            component.variables.forEach(variable => {
                                let value = null;
                                if(variable.source){
                                    if (variable.source.startsWith('~')){
                                        variable.source = path.join(component.loc, variable.source.replace("~/", "/"));
                                    }
                                    else{
                                        variable.source = path.join(vscode.workspace.rootPath, variable.source);
                                    }
                                    if(fs.existsSync(variable.source))
                                        value = fs.readFileSync(variable.source).toString();
                                    else{
                                        // Error
                                        OutputWindow.Show('Warning:', [variable.source, `This source could not be resolved as a variable for ${component.name}`], false, false);
                                    }
                                }else{
                                    value = variable.value
                                }
                                obj[`${variable.label}`] = value;
                            });
                            componentTarget = mustache.render(componentTarget, obj);
                        }

                        // remove elements
                        if(component.removeElements){
                            component.removeElements.forEach(element => {
                                const dom = new JSDOM(componentTarget);
                                (dom.window.document.querySelectorAll(element)).forEach(e => e.remove());
                                componentTarget = dom.serialize();
                            });
                        }

                        // Compress?
                        if(component.compress){
                            //componentHtml = htmlMinify(componentHtml);
                        }

                        // Save
                        FileHelper.Instance.writeToFile(targetUri, componentTarget).then((FileException: IFileResolver) => {
                                if(FileException.Exception){
                                    OutputWindow.Show('Error:', [
                                        'File save error',
                                        FileException.FileUri
                                    ], true);
                                    console.error(`error saving ${FileException.FileUri.toString()}`);
                                }
                                else {
                                    if(!this.isWatching) vscode.window.showInformationMessage('HTML Component: Build Successful');
                                    StatusBarUi.buildSuccess(this.isWatching);
                                    //OutputWindow.Show('Generated:', [FileException.FileUri], false, false);
                                }
                                //OutputWindow.Show(null, null, false, true);
                                //resolve(true);
                                resolve(FileException.FileUri);
                        });
                    }
                });
            });

            //resolve(false);
        });
    }

    private generateHTMLUri(filePath: string, componentLoc: string, target: string) {
        let generatedUri = null;
        try {
            let workspaceRoot = vscode.workspace.rootPath;

            if (filePath.startsWith('~')){
                filePath = filePath.replace("~/", "/");
                filePath = path.join(componentLoc, filePath);
            }
            else{
                filePath = path.join(workspaceRoot, filePath);
            }

            FileHelper.Instance.MakeDirIfNotAvailable(filePath);
            generatedUri = path.join(filePath, target);
        }
        catch (err) {
            console.log(err);

            OutputWindow.Show('Error:', [
                err.errno.toString(),
                err.path,
                err.message
            ], true);

            throw Error('Something Went Wrong.');
        }

        return generatedUri;
    }

    /**
     * [Deprecated]
     * Fill all HTML Components and their details from workspace (exclude exclusions from settings)
     * @param callback - callback(filepaths) with be called with details of workspace components.
     */
    //private getAllComponentsAsync(callback) {
    async getAllComponentsAsync(callback) {

        this.html_components_dir = (await this.getFiles('*/html_components/'))[0];
        let indexHtml = path.join(this.html_components_dir, 'index.html');

        if(indexHtml.length){
            let componentsUri = await this.getComponentFiles();
            if(componentsUri.length){
                let componentDetails = await componentsUri.map(this.getComponent);

                return callback(componentDetails);
            }else{
                //vscode.window.showErrorMessage("No Components found in the html_components folder.");
                OutputWindow.Show('Error:', ["No Components found in the html_components folder."], true, false);
                StatusBarUi.buildError(this.isWatching);
            }
        }else{
            //vscode.window.showErrorMessage("No Component mainframe exists. Please add an index.html to the html_components folder.");
            OutputWindow.Show('Error:', ["No Component mainframe exists. Please add an index.html to the html_components folder."], true, false);
            StatusBarUi.buildError(this.isWatching);
        }

        callback(null);
    }

    getComponent(componentUri: string) {
        let componentDetails = {};
        if (fs.existsSync(componentUri)) {
            let data = JSON.parse(fs.readFileSync(componentUri).toString());
            let name = path.basename(componentUri).substring(0, path.basename(componentUri).indexOf('.component.'));
            let loc = path.dirname(componentUri);
            let htmlUri = path.join(loc, `${name}.component.html`);
            if(!fs.existsSync(htmlUri)){
                vscode.window.showWarningMessage("Error reading component html"); // Error
            }else{
                componentDetails = {
                    name: name,
                    target: data.target? data.target: `${name}.html`,
                    loc: loc,
                    selector: data.selector? data.selector:"app-root",
                    dest: data.dest? data.dest: loc,
                    compress: data.compress? JSON.parse(data.compress):false, // parse as bool
                    htmlUri: htmlUri,
                    templatesUri: Helper.Instance.getComponentDetails(data, 'templates'),
                    tags: Helper.Instance.getComponentDetails(data, 'tags'),
                    variables: Helper.Instance.getComponentDetails(data, 'variables'),
                    inlines: Helper.Instance.getComponentDetails(data, 'inlines'),
                    removeElements: Helper.Instance.getComponentDetails(data, 'removeElements')
                } as Component;
            }

        }else{
            // Error
            vscode.window.showWarningMessage("HTML Component: Error reading Component.");
        }

        return componentDetails as Component;
    }

    private getFile(pattern: string, callback) {

        let excludedList = Helper.getConfigSettings<string[]>('excludeList');

        let options = {
            ignore: excludedList,
            mark: true,
            cwd: vscode.workspace.rootPath
        }

        glob(pattern, options, (err, files: string[]) => {
            if (err) {
                vscode.window.showInformationMessage('Error To Search Files' + err);
                callback([]);
                return;
            }
            const filePaths = files.map(file => path.join(vscode.workspace.rootPath, file));

            callback (filePaths || []);
        });
    }

    /**
     * Get files in directory that match pattern
     * @param WatchingMode WatchingMode = false for without watch mode.
     */
    private getFiles(pattern: string): Thenable<string[]> {

        let excludedList = Helper.getConfigSettings<string[]>('excludeList');

        let options = {
            ignore: excludedList,
            mark: true,
            cwd: vscode.workspace.rootPath
        }

        return new Promise(resolve => {
            glob(pattern, options, (err, files: string[]) => {
                if (err) {
                    vscode.window.showInformationMessage('Error To Search Files' + err);
                    resolve([]);
                    return;
                }

                const filePaths = files.map(file => path.join(vscode.workspace.rootPath, file));

                return resolve(filePaths || []);
            });
        })
    }

    private toggleStatusUI() {
        this.isWatching = !this.isWatching;
        let showOutputWindow = Helper.getConfigSettings<boolean>('showOutputWindow');

        if (!this.isWatching) {
            StatusBarUi.NotWatching();
            OutputWindow.Show('Not Watching...', null, showOutputWindow);
            console.log('Not Watching...');
        }
        else {
            StatusBarUi.Watching();
            OutputWindow.Show('Watching...', null, showOutputWindow);
            console.log('Watching...');
        }

    }
}
import * as vscode from 'vscode';

export class StatusBarUi {

    private static _statusBarItem: vscode.StatusBarItem;


    private static get statusBarItem() {
        if (!StatusBarUi._statusBarItem) {
            StatusBarUi._statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 200);
            this.statusBarItem.show();
        }

        return StatusBarUi._statusBarItem;
    }

    static init() {
        StatusBarUi.Working("Starting...");
        setTimeout(function(){
            StatusBarUi.NotWatching();
        },1000);
    }
    static Watching() {
        StatusBarUi.statusBarItem.text = `$(circle-slash) Watching...`;
        StatusBarUi.statusBarItem.color = 'inherit';
        StatusBarUi.statusBarItem.command = 'extension.htmlComponents.stopWatch';
        StatusBarUi.statusBarItem.tooltip = 'Stop auto build of HTML Components';
    }
    static NotWatching() {
        StatusBarUi.statusBarItem.text = `$(gist) Watch HTML`;
        StatusBarUi.statusBarItem.color = 'inherit';
        StatusBarUi.statusBarItem.command = 'extension.htmlComponents.startWatch';
        StatusBarUi.statusBarItem.tooltip = 'Auto build of HTML Components';
    }
    static Working(workingMsg:string = "Working on it...") {
        StatusBarUi.statusBarItem.text = `$(zap) ${workingMsg}`;
        StatusBarUi.statusBarItem.tooltip = 'In case if it takes long time, Show output window and report.';
        //StatusBarUi.statusBarItem.command = null;
    }

    // Quick status bar messages after build success or error
    static buildSuccess(isWatching: boolean) {
        StatusBarUi.statusBarItem.text = `$(check) Success`;
        StatusBarUi.statusBarItem.color = '#33ff00';

        if(isWatching) {
            setTimeout( function() {
                StatusBarUi.statusBarItem.color = 'inherit';
                StatusBarUi.Watching();
            }, 1000);
        }
        else {
            StatusBarUi.NotWatching();
        }
    }
    static buildError(isWatching: boolean) {
        StatusBarUi.statusBarItem.text = `$(x) Error`;
        StatusBarUi.statusBarItem.color = '#ff0033';
        //StatusBarUi.statusBarItem.command = null;

        if(isWatching) {
            setTimeout( function() {
                StatusBarUi.statusBarItem.color = 'inherit';
                StatusBarUi.Watching();
            }, 2500);
        }
        else {
            StatusBarUi.NotWatching();
        }
    }

    static dispose() {
        StatusBarUi.statusBarItem.dispose();
    }
}
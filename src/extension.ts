'use strict';

import { ExtensionContext, workspace, commands } from 'vscode';
import { App } from './App';


export function activate(context: ExtensionContext) {

    console.log('"html-components" is now active!');

    let app = new App();

	context.subscriptions.push(commands
        .registerCommand('extension.htmlComponents.startWatch', async () => {
			app.startWatch();
        })
    );

    context.subscriptions.push(commands
        .registerCommand('extension.htmlComponents.stopWatch', () => {
            app.stopWatch();
        })
    );

    context.subscriptions.push(commands
        .registerCommand('extension.htmlComponents.buildComponent', () => {
            app.buildAllComponents();
        })
    );

    context.subscriptions.push(workspace
        .onDidSaveTextDocument(() => {
            app.buildOnSave();
        })
    );

    context.subscriptions.push(app as any);
}

// this method is called when your extension is deactivated
export function deactivate() {}

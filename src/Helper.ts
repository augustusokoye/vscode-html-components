import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import { URL } from "url";

var splitLines = require('split-lines');
var glob = require('glob-fs')({ gitignore: true });

/**
 * Valid types for path values in "fs".
 */
type PathLike = string | Buffer | URL;


export interface ComponentTemplate{
    selector: string;
    source: string;
}
export interface ComponentTagAttribute{
    attributeLabel: string;
    value: string;
}
export interface ComponentInline{
    selector: string;
    source: string;
    attributes?: ComponentTagAttribute[];
}
export interface ComponenAnyTag{
    tag: string;
    attributes: ComponentTagAttribute[];
}
export interface ComponentTag{
    selector: string;
    scripts?: string[];
    links?: string[];
    any?: ComponenAnyTag[];
}
export interface ComponentVariable{
    label: string;
    value?: string;
    source?: string;
}

export interface Component{
    name: string;
    target: string;
    loc: string;
    selector: string;
    htmlUri: string;
    compress?: boolean;
    dest: string;
    templatesUri?: ComponentTemplate[];
    tags?: ComponentTag[];
    variables?: ComponentVariable[];
    inlines?: ComponentInline[];
    removeElements?: string[];
}

export interface IFileResolver {
    FileUri: string,
    Exception: NodeJS.ErrnoException
}

export class Helper {

    public static get Instance() {
        return new Helper();
    }

    private static get configSettings() {
        return vscode.workspace.getConfiguration('htmlComponents.settings');
    }
    static getConfigSettings<T>(val: string): T {
        return this.configSettings.get(val) as T;
    }


    getComponentDetails(componentData: any, label: string): any{
        switch(label){
            case "templates":
                let templates = Array();
                if(componentData.templates){
                    for(var i=0; i<componentData.templates.length; i++)
                    {
                        templates.push({
                            selector: componentData.templates[i].selector,
                            source: componentData.templates[i].source
                        } as ComponentTemplate);
                    }
                }
            return templates as ComponentTemplate[];

            case "tags":
                if(componentData.tags){
                    return componentData.tags.map(tag => {
                        let thisTag = new Object();

                        thisTag["selector"] = tag.selector;
                        if(tag.scripts){
                            thisTag["scripts"] = Array();
                            for(var i=0; i<tag.scripts.length; i++)
                                thisTag.scripts.push(tag.scripts[i]);
                        }
                        if(tag.links){
                            thisTag["links"] = Array();
                            for(var i=0; i<tag.links.length; i++)
                                thisTag.links.push(tag.links[i]);
                        }
                        if(tag.any){
                            thisTag["any"] = Array();
                            for(var i=0; i<tag.any.length; i++)
                                thisTag.any.push(tag.any[i]);
                        }

                        return thisTag;
                    }) as ComponentTag[];
                }
                else return null;

            case "variables":
                let variables = new Array();
                if(componentData.variables){
                    for(var i=0; i<componentData.variables.length; i++)
                    {
                        variables.push({
                            label: componentData.variables[i].label,
                            value: componentData.variables[i].value,
                            source: componentData.variables[i].source
                        } as ComponentVariable);
                    }
                }
            return variables as ComponentVariable[];

            case "inlines":
                let inlines = new Array();
                inlines["selector"] = componentData.inlines.selector;
                if(componentData.inlines){
                    for(var i=0; i<componentData.inlines.length; i++)
                    {
                        inlines.push({
                            selector: componentData.inlines[i].selector,
                            source: componentData.inlines[i].source,
                            attributes: componentData.inlines[i].attributes
                        } as ComponentInline);
                    }
                }
            return inlines as ComponentInline[];

            case "removeElements":
                let removeElements = new Array();
                if(componentData.removeElements){
                    for(var i=0; i<componentData.removeElements.length; i++)
                    {
                        removeElements.push(componentData.removeElements[i]);
                    }
                }
            return removeElements as string[];
        }
    }


    placeLines(main: string, selector: string, lines: string[]): string{

        let tag = `<${selector}></${selector}>`;

        (splitLines(main) as string[]).forEach((mainLine, mainLineIndex) => {
            if(mainLine.includes(selector)){
                var regExpr = /[^a-zA-Z0-9-. ]/g; // Regex replace/escape special characters
                let padding = mainLine.substring(0, mainLine.indexOf(selector)).replace(regExpr, '');;
                main = main.replace(tag, lines.map((line,i) => (i? padding:'')+line+(i<lines.length-1? '\n':'')).join(''));
            }
        });

        return main;
    }
    place(main: string, selector: string, data: string): string{
        let dataLines = splitLines(data) as string[];
        return this.placeLines(main, selector, dataLines);
    }

    timeDifference_ms(dt2: Date, dt1: Date)
    {
        var diff = dt2.getTime() - dt1.getTime();
        return Math.abs(Math.round(diff));
    }
    timeInfo(time: Date)
    {
        return (time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate()+' '+
                time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds());
    }
}

export class FileHelper {

    public static get Instance() {
        return new FileHelper();
    }

    writeToFile(targetFileUri: PathLike | number, data: any) {
        return new Promise<IFileResolver>((resolve) => {
            fs.writeFile(targetFileUri, data, 'utf8', (err) => {
                resolve({
                    FileUri : targetFileUri,
                    Exception: err
                } as IFileResolver);
            });
            return
        })

    }

    MakeDirIfNotAvailable(dir: string) {
        if (fs.existsSync(dir)) return;
        if (!fs.existsSync(path.dirname(dir))) {
            this.MakeDirIfNotAvailable(path.dirname(dir));
        }
        fs.mkdirSync(dir);
    }
}

# HTML Components

**_If you like HTML Components [please leave a review](https://marketplace.visualstudio.com/items?itemName=augustusokoye.html-components#review-details), it does put a smile on my face._**

[![VSCode Marketplace Badge](https://img.shields.io/vscode-marketplace/v/augustusokoye.html-components.svg?label=VSCode%20Marketplace&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=augustusokoye.html-components) [![Total Install](https://img.shields.io/vscode-marketplace/d/augustusokoye.html-components.svg?style=flat-square)](https://marketplace.visualstudio.com/items?itemName=augustusokoye.html-components) [![Avarage Rating Badge](https://img.shields.io/vscode-marketplace/r/augustusokoye.html-components.svg?style=flat-square)](https://marketplace.visualstudio.com/items?itemName=augustusokoye.html-components) [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://github.com/ritwickdey/vscode-live-sass-compiler/)

HTML web development using Components approach with auto rebuild.

Design pattern inspired by Ritwick Dey's [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) and [Live Sass Compiler](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass) extensions.


## Features
* Auto (re)build HTML Components.
* Start or stop auto rebuild by a single click from status bar.
* Change detection and auto rebuild.
* Render template files as `<selector></selector>` with relative indenting.
* Render predefined variables as `{{ var }}`.
* Render external text files as `{{ var }}`.
* Include external script files as inline scripts. Support for tag attributes.
* Specify `script` and `links` tags. E.g.
  `<script src="/script.js"></script>`,
  `<link rel="stylesheet" type="text/css" href="/link.css">`. Alternative declare `any` tag with specific attributes.
* Define specific HTML elements to be omitted from output HTML.
* Indipendently customisable output dest for exported HTML.
* Auto-creates target directory if one does not exist.
* Customisable target name.
* Compress (`minify`) output HTML.
* Exclude specific folders by settings.
* Quick status bar control.


## Usage
1. Create a `html_components` folder anywhere in your workspace.
2. Add `index.html` in the folder. This is the output HTML mainframe.
3. Create a `html_components/templates` folder for project Component templates.
   _Note that templates outside this folder will not be detected._
4. Create a `html_components/components` folder for project Components.
5. Create `app.component.html` and `app.component.json` in the components folder. A Component's `html` and `json` must be placed in the same directory. Component (html and json) may be placed together in any directory or subdirectory.
6. A Component's `json` defines the Component parameters. See [Example](./docs/component-example.md).
    * Use `templates` to specify external html(s) to be placed in a selector tag as text.
    * Use `inlines` to specify external script(s) to be placed in a script tag.
    * Use `tags` to specify a script, link or `any` tag with custom attributes.
    * `variables` is used to replace a {{ var }} with a specified value or sourced from an external file.
    * Use `removeElements` to specify html elements to be ommitted from the output html.
7. Specify relative paths using `~/` and workspace paths using `./` or `/`.

> Typical project directory layout:

    .
    ├── dist                                # Build output files
    ├── docs                                # Documentation files
    ├── src                                 # Source files (alternatively `lib` or `app`)
    |   ├── html_components
    |   |   ├── components
    |   |   |   ├── app1.component.html
    |   |   |   ├── app1.component.json
    |   |   |   └── app2
    |   |   |       ├── app2.component.html
    |   |   |       └── app2.component.json
    |   |   ├── templates
    |   |   └── index.html
    |   ├── scripts                         # Project typescripts to compile to dist/js/
    │   └── scss                            # Project scss to compile to dist/css/
    ├── tools                               # Tools and utilities
    ├── LICENSE
    └── README.md


## Shortcuts

1. Open a project and click `Watch HTML` from the status bar to turn on auto rebuild. Click again to turn off auto rebuild.
2. Use `(alt+H, alt+S)` to turn on auto rebuild and `(alt+H, alt+P)` to turn off auto rebuild. *[On MAC, `cmd+H, cmd+S` and `cmd+H, cmd+P`]*
3. Use `(alt+H, alt+B)` to build all Components. *[On MAC, `cmd+H, cmd+B`]*
4. You can also use the Command Pallete by pressing `F1` or `ctrl+shift+P` and type `HTML Components: Start Watch` turn on auto rebuild, `HTML Components: Stop Watch` turn off auto rebuild or type `HTML Components: Build Components` to build all Components.


## Installation
Open VSCode and type `ctrl+P`, type `ext install html-components`.


## Settings
All settings are listed [here](./docs/settings.md).


## Test
See a test project [here](./docs/test/).


## Web Development Recommendations

* [sftp](https://marketplace.visualstudio.com/items?itemName=liximomo.sftp) SFTP/FTP syncing
* [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) for local Server with live reload feature for static & dynamic pages
* [Live Sass Compiler](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass) helps compile/transpile SASS/SCSS files to CSS files at realtime with live browser reload
* [Sass](https://marketplace.visualstudio.com/items?itemName=robinbentley.sass-indented) syntax highlighting, autocomplete & snippets
* [Auto Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag) sutomatically adds HTML/XML close tags
* [Beautify](https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify) beautifies javascript, JSON, CSS, Sass, and HTML
* [VS Color Picker](https://marketplace.visualstudio.com/items?itemName=lihui.vs-color-picker) provides embedded GUI to generate color codes for CSS/SASS/SCSS
* [emply-indent](https://marketplace.visualstudio.com/items?itemName=DmitryDorofeev.empty-indent) removes indent of empty lines on save
* [Highlight Matching Tag](https://marketplace.visualstudio.com/items?itemName=vincaslt.highlight-matching-tag) hightlights HTML/XML tags


## LICENSE
This extension is licensed under the [MIT License](LICENSE)



**_If you found any bugs or you have any suggestions, feel free to contact me._**

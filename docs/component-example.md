
## Example Component json

### app.component.json

Remember, a Component's html (`app.component.html`) must be places alongside the Component's json (`app.component.json`) in the `html_components` folder.
Inside of `app.component.json`, use the following example.

`app-root` specifies to placement position of the Component's html.

```json
{
    "target": "index.html",
    "dest": "/dest/",
    "selector": "app-root",
    "compress": false,
    "templates": [
        {
            "selector": "app-header",
            "source": "template-header.html"
        },
        {
            "selector": "app-footer",
            "source": "template-footer.html"
        }
    ],
    "inlines": [
        {
            "selector": "inline-1",
            "source": "~/inline-1.json",
            "attributes": [
                {
                    "tagLabel": "type",
                    "value": "application/ld+json"
                }
            ]
        },
        {
            "selector": "app-script",
            "source": "~/inline-2.js"
        }
    ],
    "tags":[
        {
            "selector": "app-head-tags",
            "scripts": [
                "/js/jquery.js",
                "/js/script.js"
            ],
            "links": [
                "/css/bootstrap.css",
                "/css/style.css"
            ],
            "any": [
                {
                    "tag": "link",
                    "attributes": [
                        {
                            "attributeLabel": "rel",
                            "value": "stylesheet"
                        },
                        {
                            "attributeLabel": "type",
                            "value": "text/css"
                        },
                        {
                            "attributeLabel": "href",
                            "value": "/css/any-link-tag-test.css"
                        }
                    ]
                }
            ]
        },
        {
            "selector": "app-footer-tags",
            "scripts": [
                "/js/popper.js",
                "/js/bootstrap.js",
                "/js/holder.js"
            ]
        }
    ],
    "variables": [
        {
            "label": "sitecode",
            "value": "au"
        },
        {
            "label": "subdomain",
            "value": "/au"
        },
        {
            "label": "title",
            "value": "HTML Components Generated Site"
        },
        {
            "label": "keywords",
            "source": "~/keywords.md"
        },
        {
            "label": "description",
            "source": "~/description.md"
        },
        {
            "label": "og_description",
            "source": "~/description.md"
        },
        {
            "label": "locale",
            "value": "en_AU"
        }
    ],
    "removeElements": [
        ".app-test-remove-class",
        "#app-test-remove-id",
        "app-test-remove-element"
    ]
}
```
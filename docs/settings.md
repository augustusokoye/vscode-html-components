## Settings


* **`htmlComponents.settings.excludeList`:** To Exclude specific folders. All files inside the folders will be ignored.
    * _default value :_
        ```json
            "htmlComponents.settings.excludeList": [
                "**/node_modules/**",
                ".vscode/**"
            ]
        ```
    * You can use negative glob pattern.

        * _Example : if you want exclude all file except `file1.json` & `file2.json` from `path/subpath` directory, you can use the expression -_

        ```json
        "htmlComponents.settings.excludeList": [
            "path/subpath/*[!(file1|file2)].json"
        ]
        ```

    <hr>

* **`htmlComponents.settings.autoSaveAll` :** Auto save all files in the `html_components` folder.
    * *Default value is `true`*

     <hr>

* **`htmlComponents.settings.showOutputWindow` :** Set this to `false` if you do not want the output window to show.
    * *NOTE: You can use the command palette to open the Live Sass output window.*
    * *Default value is `true`*

     <hr>
